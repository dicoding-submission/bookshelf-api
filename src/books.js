/*
const books = [
    {
        id: "1dWpfp4H2oA81AxX",
        name: "BukuPertama",
        year: 2011,
        author: "author pertama",
        summary: "deskripsi buku pertama",
        publisher: "publisher pertama",
        pageCount: 100,
        readPage: 0,
        reading: true,
        finished: false,
        insertedAt: "2023-04-06T22:22:37.485Z",
        updatedAt: "2023-04-06T22:22:37.485Z"
    },
    {
        id: "COYzd-xMHWAr9U_F",
        name: "BukuKedua",
        year: 2012,
        author: "author kedua",
        summary: "deskripsi buku kedua",
        publisher: "publisher kedua",
        pageCount: 100,
        readPage: 10,
        reading: false,
        finished: false,
        insertedAt: "2023-04-06T22:22:37.485Z",
        updatedAt: "2023-04-06T22:22:37.485Z"
    },
    {
        id: "ep8gH5mHgF6JXe76",
        name: "Buku Ketiga",
        year: 2013,
        author: "author ketiga",
        summary: "deskripsi buku ketiga",
        publisher: "publisher ketiga",
        pageCount: 100,
        readPage: 100,
        reading: false,
        finished: true,
        insertedAt: "2023-04-06T22:22:37.485Z",
        updatedAt: "2023-04-06T22:22:37.485Z"
    },
    {
        id: "6AlKkHN7MLtrkUM0",
        name: "Buku Keempat ty",
        year: 2014,
        author: "author keempat",
        summary: "deskripsi buku keempat",
        publisher: "publisher keempat",
        pageCount: 100,
        readPage: 100,
        reading: true,
        finished: true,
        insertedAt: "2023-04-06T22:22:37.485Z",
        updatedAt: "2023-04-06T22:22:37.485Z"
    },
];
*/
const books = []
module.exports = books
